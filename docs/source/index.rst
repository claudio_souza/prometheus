.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Pipeline Machine Learning

   etl
   treino
   deploy

***************
Prometheus
***************

**Prometheus** é o projeto de benchmark de planos, operadoras e empresas que compõem o rol de parceiros e clientes Dasa Empresas.

.. admonition:: Benchmark (*substantivo*)

   Padrão ou ponto de referência contra o qual algo pode ser comparado ou avaliado.


O projeto é dividido nos seguintes componentes:
    #. cálculo do "perfil de saúde" de clientes e colaboradores;
    #. agrupamento de clientes e colaboradores baseado na semelhança de seus perfis de saúde;
    #. cálculo do "perfil médio de saúde" (**benchmark**) de cada grupo.

A partir do cálculo dos benchmarks podemos avaliar os perfis de saúde dos clientes e desenvolver planos de ação para casos de sub-utilização, sobre-utilização e riscos de saúde.


Cálculo do Perfil de Saúde
--------------------------

O perfil de saúde é calculado com base nos indicadores de utilização apresentados na tabela a seguir:

.. table::

   +--------------------------------------------------------------------+--------------------------------------+----------------------------------------+-----------------------------------------------------------+
   | Vidas                                                              | Ambulatório                          | Internação                             | Financeiro                                                |
   +====================================================================+======================================+========================================+===========================================================+
   | % de beneficiários Alto Custo                                      | Consultas por beneficiário           | Preço Médio de Internação              | Sinistro por grupo de despesa                             |
   +--------------------------------------------------------------------+--------------------------------------+----------------------------------------+-----------------------------------------------------------+
   | % de beneficiários Crônicos                                        | Exames por beneficiário              | Taxa de Internação                     | Sinistro per capita                                       |
   +--------------------------------------------------------------------+--------------------------------------+----------------------------------------+-----------------------------------------------------------+
   | % de beneficiários Contumazes (PS, Eletivas e Exames)              | Exames por consulta                  | Permanência Média                      | Prêmio per capita                                         |
   +--------------------------------------------------------------------+--------------------------------------+----------------------------------------+-----------------------------------------------------------+
   | % de Gestantes                                                     | Psicoterapias por beneficiário       | % UTI Neonatal                         | % de arrecadação de coparticipação em relação ao sinistro |
   +--------------------------------------------------------------------+--------------------------------------+----------------------------------------+-----------------------------------------------------------+
   | % de Crônicos de Alto Custo                                        | % Consultas Médicas de Urgência      | % Internações curtas (2 dias ou menos) |                                                           |
   +--------------------------------------------------------------------+--------------------------------------+----------------------------------------+-----------------------------------------------------------+
   | % de Não Utilizadores e Não Utilizadores de Protocolos Preventivos | % de conversão de PS para Internação | % Internações com UTI                  |                                                           |
   +--------------------------------------------------------------------+--------------------------------------+----------------------------------------+-----------------------------------------------------------+
   |                                                                    |                                      | % de Reinternação                      |                                                           |
   +--------------------------------------------------------------------+--------------------------------------+----------------------------------------+-----------------------------------------------------------+

**Obs 1:** Para o benchmark de empresas, apenas o os indicadores de vidas são calculados.

**Obs 2:** Para os benchmarks de planos e operadoras, apenas os indicadores de ambulatório, internação e financeiro são calculados.


Agrupamento por Perfil de Saúde
-------------------------------

Tendo calculado os perfis de saúde para cada cliente e colaborador Dasa Empresas, o agrupamento desses perfis é feito utilizando algoritmos de machine learning conhecidos como algoritmos de `clusterização <https://scikit-learn.org/stable/modules/clustering.html>`_.

Em termos gerais, a semelhança entre os perfis é calculada com base na "distância" entre os indicadores que compõem os perfis (quanto mais "perto", mais semelhantes).

Ao final do agrupamento, idealmente queremos ter grupos (*clusters*) bem separados e com um formato bem definido, como na imagem a seguir.

.. figure:: https://i.stack.imgur.com/7HFxw.jpg
    :width: 35 em
    :align: center

    Exemplo visual de clusterização com dois indicadores


Benchmark: Perfil Médio de Saúde
--------------------------------

Em machine learning, o ponto médio de um cluster é chamado centróide. Na figura acima, os centróides de cada grupo são representados pelo ponto |black-x-inside-circle|.

.. |black-x-inside-circle| image:: https://www.shareicon.net/data/512x512/2015/10/21/659823_cross_512x512.png
   :height: 11
   :width: 11

O "perfil médio de saúde" é o resultado da média dos indicadores da população que compõe cada grupo.
Logo, ao calcularmos os centróides, também teremos calculado o perfil médio de saúde de cada grupo.