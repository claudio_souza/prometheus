******
Treino
******

O pipeline de treino é dividido em 3 etapas principais, descritas a seguir.


Formatação do dataset preliminar de treino
------------------------------------------
Na primeira etapa, temos a formação preliminar do dataset que vai servir de insumo para o modelo. \
Em machine learning, este dataset é geralmente chamado ``X``.

Para "montar" ``X``, precisamos remover todas as colunas que não servem para a clusterização.

.. code-block::

    X = df.fillna(0).drop(
        columns=[
            'idEmpresa', 'dtCompetencia', 'flag_indicador_empresa',
            'IdApoliceSubFaturaPlano', 'dtCompetenciaContapaga', 'flag_indicador_plano',
            'IdOperadora', 'flag_indicador_operadora'
        ],
        errors='ignore'
    )


Pré-processamento
-----------------
Nesta etapa, precisamos enriquecer o dataset X, para que o modelo de clusterização \
tenha mais informação e um melhor aproveitamento na hora de detectar os agrupamentos.

Para isso, submetemos o dataset X ao pipeline definido a seguir:

.. code-block::

    preprocessing_pipe = Pipeline([
        ("feature_engineering", PolynomialFeatures(degree=50, include_bias=False, interaction_only=True)),
        ("scale_0_1", MinMaxScaler()),
        ("discretize", Quantizer()),
        ("pca", PCA(n_components=3, random_state=42))
    ])

    preprocessed_X = preprocessing_pipe.fit_transform(X)


Treino do modelo
----------------
A seguir, passamos para a etapa mais esperada: o treino do modelo de clusterização. \
Ironicamente, esta é a parte mais simples do processo.

O modelo que utilizamos é o clássico ``KMeans``, e após algumas análises, definimos o número de clusters = 5.
A seguir, temos a definição completa do modelo.

.. code-block::

    clustering_pipe = Pipeline([
        ("cluster", KMeans(n_clusters=5, n_init=50, max_iter=500, random_state=42))
    ])

    clusters = clustering_pipe.fit_predict(preprocessed_X)


Resultado
---------
O resultado da clusterização é salvo no BigQuery, na tabela ``fenix_clean.benchmark``.

A seguir, podemos ver o gráfico de clusters gerado pelo treino mais recente.
**Talvez precise buildar a documentação novamente para atualizar as imagens**.

.. image:: img/clusters_empresas.png

.. image:: img/clusters_planos.png

.. image:: img/clusters_operadoras.png
