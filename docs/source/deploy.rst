******
Deploy
******

.. highlight:: sh

Para fazer o deploy, temos dois scripts (um para deploy em dev, outro em prod).
Eles estão salvos na pasta ``cloud_run_deploy``, que se encontra na raiz do projeto.

O deploy consiste das seguintes 3 etapas:

1. Autenticação com o GCP

.. code-block::

    gcloud auth activate-service-account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
    --key-file src/config/gcloud_credentials_dev.json

2. Build da imagem docker no Google Cloud Registry

.. code-block::

    gcloud builds submit \
    --tag gcr.io/laboratorio-eng-dados/dev-prometheus \
    --account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
    --project laboratorio-eng-dados

3. Deploy do container no Cloud Run

.. code-block::

    gcloud run deploy \
    dev-prometheus \
    --image gcr.io/laboratorio-eng-dados/dev-prometheus \
    --allow-unauthenticated \
    --platform managed \
    --region us-east1 \
    --account preditivo-dev@laboratorio-eng-dados.iam.gserviceaccount.com \
    --project laboratorio-eng-dados \
    --memory 1Gi \
    --timeout 10m

Para fazer o deploy, basta executar os seguintes comandos no terminal:

.. code-block::

    >> ./dev-deploy.sh
    >> ./prd-deploy.sh