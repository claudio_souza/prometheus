import os
import logging
from src.data_ingestion.utils import run_query_on_bq

query_perfil_por_empresa = f"""
    SELECT 
        tb1.idEmpresa, tb1.dtCompetencia, COUNT(DISTINCT idPessoa) AS qtd_vidas,
        
        SUM(AltoCusto_Flag) / COUNT(DISTINCT IdPessoa)                              AS perc_alto_custo,
        SUM(Cronico_Flag) / COUNT(DISTINCT IdPessoa)                                AS perc_cronicos,
        SUM(ContumazConsultaEletiva) / COUNT(DISTINCT IdPessoa)                     AS perc_contumazes_consultas_eletivas,
        SUM(ContumazConsultaPS) / COUNT(DISTINCT IdPessoa)                          AS perc_contumazes_ps,
        SUM(ContumazExames) / COUNT(DISTINCT IdPessoa)                              AS perc_contumazes_exames,
        SUM(AltoCusto_Gestante) / COUNT(DISTINCT IdPessoa)                          AS perc_gestantes,
        SUM(IF(Cronico_Flag + AltoCusto_Flag > 0, 1, 0)) / COUNT(DISTINCT IdPessoa) AS perc_cronicos_alto_custo,
        SUM(NaoUtilizadoresFaixaEtaria40a69) / COUNT(DISTINCT IdPessoa)             AS perc_NaoUtilizadoresFaixaEtaria40a69,
        SUM(NaoUtilizadoresFaixaEtaria21a65) / COUNT(DISTINCT IdPessoa)             AS perc_NaoUtilizadoresFaixaEtaria21a65,
        SUM(NaoUtilizadoresfaixaEtaria50a75) / COUNT(DISTINCT IdPessoa)             AS perc_NaoUtilizadoresfaixaEtaria50a75,
        SUM(NaoUtilizadoresFaixaEtariaMenor2) / COUNT(DISTINCT IdPessoa)            AS perc_NaoUtilizadoresFaixaEtariaMenor2,
        SUM(NaoUtilizadoresFaixaEtariaMaior45) / COUNT(DISTINCT IdPessoa)           AS perc_NaoUtilizadoresFaixaEtariaMaior45,
        SUM(NaoUtilizadoresNUMamografia) / COUNT(DISTINCT IdPessoa)                 AS perc_NaoUtilizadoresNUMamografia,
        SUM(NaoUtilizadoresNUPapanicolau) / COUNT(DISTINCT IdPessoa)                AS perc_NaoUtilizadoresNUPapanicolau,
        SUM(NaoUtilizadoresNUSangueOculto) / COUNT(DISTINCT IdPessoa)               AS perc_NaoUtilizadoresNUSangueOculto,
        SUM(NaoUtilizadoresNULactentes) / COUNT(DISTINCT IdPessoa)                  AS perc_NaoUtilizadoresNULactentes,
        SUM(NaoUtilizadoresNUCheckUp) / COUNT(DISTINCT IdPessoa)                    AS perc_NaoUtilizadoresNUCheckUp,
        1                                                                           AS flag_indicador_empresa
        
    FROM
        {os.getenv('GCLOUD_PROJECT')}.fenix_bi.view_fato_classificacao_vidas AS tb1
    
    INNER JOIN (
        SELECT 
            idEmpresa, MAX(dtCompetencia) AS dtCompetencia
        FROM
            {os.getenv('GCLOUD_PROJECT')}.fenix_bi.view_fato_classificacao_vidas
        GROUP BY idEmpresa
    ) AS tb2 
        ON tb1.idEmpresa = tb2.idEmpresa AND tb1.dtCompetencia = tb2.dtCompetencia
    
    GROUP BY tb1.idEmpresa, tb1.dtCompetencia
    ORDER BY tb1.idEmpresa, tb1.dtCompetencia
"""

query_perfil_por_plano = f"""
    SELECT 
    
        tb1.IdApoliceSubFaturaPlano, tb1.dtCompetenciaContapaga, COUNT(DISTINCT idBeneficiario) AS qtd_vidas,
        
        -- Indicadores ambulatoriais.
        SUM(IF(AMB_dsClassPassagem = "Consulta Eletiva", 1, 0)) / COUNT(DISTINCT idBeneficiario)                                 AS consultas_eletivas_por_beneficiario,
        SUM(IF(AMB_dsClassPassagem = "Pronto-Socorro", 1, 0)) / COUNT(DISTINCT idBeneficiario)                                   AS consultas_ps_por_beneficiario,
        SUM(IF(AMB_dsClassPassagem = "Exames" AND AMBINT_dsCategoriaExame = "Simples", 1, 0)) / COUNT(DISTINCT idBeneficiario)   AS exames_simples_por_beneficiario,
        SUM(IF(AMB_dsClassPassagem = "Exames" AND AMBINT_dsCategoriaExame = "Especiais", 1, 0)) / COUNT(DISTINCT idBeneficiario) AS exames_especiais_por_beneficiario,
        SUM(IF(AMBINT_dsTipoTerapia = "Psicologia", 1, 0)) / COUNT(DISTINCT idBeneficiario)                                      AS psicoterapias_por_beneficiario,
        
        -- TODO: exames_por_consulta_ps
        -- TODO: exames_por_consulta_eletiva
        -- TODO: perc_consultas_de_urgencia
        -- TODO: perc_conversao_ps_internacao
            
        -- Indicadores de internação.
        SAFE_DIVIDE(SUM(INT_vrPagoProcedimentoContaPaga_Internacao), COUNT(DISTINCT INT_IdPassagem_Internacao))                  AS preco_medio_internacao,
        SAFE_DIVIDE(COUNT(DISTINCT INT_IdPassagem_Internacao), COUNT(DISTINCT idBeneficiario))                                   AS taxa_internacao,
        SAFE_DIVIDE(SUM(IF(INT_FlagNeoNatal = "Sim", 1, 0)), SUM(IF(INT_FlagParto = "Sim", 1, 0)))                               AS perc_uti_neonatal_x_partos,
        SAFE_DIVIDE(SUM(IF(INT_FlagINTernacaoUTI = "Sim", 1, 0)), COUNT(DISTINCT INT_IdPassagem_Internacao))                     AS perc_interncoes_uti,
            
        -- TODO: permanencia_media_internacao
        -- TODO: perc_internacoes_curtas
        -- TODO: perc_reinternacoes
            
        -- Indicadores financeiros.
        SUM(vrPagoProcedimentoContaPaga) / COUNT(DISTINCT idBeneficiario)                                                        AS sinistro_per_capita,
        SAFE_DIVIDE(SUM(vrCoParticipacaoProcedimentoContaPaga), SUM(vrPagoProcedimentoContaPaga))                                AS perc_copart_sinistro,
        
        -- TODO: sinistro_por_grupo_despesa
        -- TODO: premio_per_capita
        
        1                                                                                                                        AS flag_indicador_plano
    
    FROM
        {os.getenv('GCLOUD_PROJECT')}.fenix_bi.view_fato_sinistro AS tb1
    
    INNER JOIN (
        SELECT 
            IdApoliceSubFaturaPlano, MAX(dtCompetenciaContapaga) AS dtCompetenciaContapaga
        FROM
            {os.getenv('GCLOUD_PROJECT')}.fenix_bi.view_fato_sinistro
        GROUP BY IdApoliceSubFaturaPlano
    ) AS tb2 
        ON tb1.IdApoliceSubFaturaPlano = tb2.IdApoliceSubFaturaPlano AND tb1.dtCompetenciaContapaga = tb2.dtCompetenciaContapaga
    
    GROUP BY tb1.IdApoliceSubFaturaPlano, tb1.dtCompetenciaContapaga
    ORDER BY tb1.IdApoliceSubFaturaPlano, tb1.dtCompetenciaContapaga
"""

query_perfil_por_operadora = f"""
    SELECT 
    
        tb1.IdOperadora, tb1.dtCompetenciaContapaga, COUNT(DISTINCT idBeneficiario) AS qtd_vidas,
        
        -- Indicadores ambulatoriais.
        SUM(IF(AMB_dsClassPassagem = "Consulta Eletiva", 1, 0)) / COUNT(DISTINCT idBeneficiario)                                 AS consultas_eletivas_por_beneficiario,
        SUM(IF(AMB_dsClassPassagem = "Pronto-Socorro", 1, 0)) / COUNT(DISTINCT idBeneficiario)                                   AS consultas_ps_por_beneficiario,
        SUM(IF(AMB_dsClassPassagem = "Exames" AND AMBINT_dsCategoriaExame = "Simples", 1, 0)) / COUNT(DISTINCT idBeneficiario)   AS exames_simples_por_beneficiario,
        SUM(IF(AMB_dsClassPassagem = "Exames" AND AMBINT_dsCategoriaExame = "Especiais", 1, 0)) / COUNT(DISTINCT idBeneficiario) AS exames_especiais_por_beneficiario,
        SUM(IF(AMBINT_dsTipoTerapia = "Psicologia", 1, 0)) / COUNT(DISTINCT idBeneficiario)                                      AS psicoterapias_por_beneficiario,
        
        -- TODO: exames_por_consulta_ps
        -- TODO: exames_por_consulta_eletiva
        -- TODO: perc_consultas_de_urgencia
        -- TODO: perc_conversao_ps_internacao
            
        -- Indicadores de internação.
        SAFE_DIVIDE(SUM(INT_vrPagoProcedimentoContaPaga_Internacao), COUNT(DISTINCT INT_IdPassagem_Internacao))                  AS preco_medio_internacao,
        SAFE_DIVIDE(COUNT(DISTINCT INT_IdPassagem_Internacao), COUNT(DISTINCT idBeneficiario))                                   AS taxa_internacao,
        SAFE_DIVIDE(SUM(IF(INT_FlagNeoNatal = "Sim", 1, 0)), SUM(IF(INT_FlagParto = "Sim", 1, 0)))                               AS perc_uti_neonatal_x_partos,
        SAFE_DIVIDE(SUM(IF(INT_FlagINTernacaoUTI = "Sim", 1, 0)), COUNT(DISTINCT INT_IdPassagem_Internacao))                     AS perc_interncoes_uti,
            
        -- TODO: permanencia_media_internacao
        -- TODO: perc_internacoes_curtas
        -- TODO: perc_reinternacoes
            
        -- Indicadores financeiros.
        SUM(vrPagoProcedimentoContaPaga) / COUNT(DISTINCT idBeneficiario)                                                        AS sinistro_per_capita,
        SAFE_DIVIDE(SUM(vrCoParticipacaoProcedimentoContaPaga), SUM(vrPagoProcedimentoContaPaga))                                AS perc_copart_sinistro,
        
        -- TODO: sinistro_por_grupo_despesa
        -- TODO: premio_per_capita
        
        1                                                                                                                        AS flag_indicador_operadora
    
    FROM
        {os.getenv('GCLOUD_PROJECT')}.fenix_bi.view_fato_sinistro AS tb1
    
    INNER JOIN (
        SELECT 
            IdOperadora, MAX(dtCompetenciaContapaga) AS dtCompetenciaContapaga
        FROM
            {os.getenv('GCLOUD_PROJECT')}.fenix_bi.view_fato_sinistro
        GROUP BY IdOperadora
    ) AS tb2 
        ON tb1.IdOperadora = tb2.IdOperadora AND tb1.dtCompetenciaContapaga = tb2.dtCompetenciaContapaga
    
    GROUP BY tb1.IdOperadora, tb1.dtCompetenciaContapaga
    ORDER BY tb1.IdOperadora, tb1.dtCompetenciaContapaga
"""


def get_perfil_por_empresa():
    logging.debug("Extraindo dados do BigQuery: perfil por empresa.")
    results = run_query_on_bq(query_perfil_por_empresa)
    return results


def get_perfil_por_plano():
    logging.debug("Extraindo dados do BigQuery: perfil por plano.")
    results = run_query_on_bq(query_perfil_por_plano)
    return results


def get_perfil_por_operadora():
    logging.debug("Extraindo dados do BigQuery: perfil por operadora.")
    results = run_query_on_bq(query_perfil_por_operadora)
    return results
