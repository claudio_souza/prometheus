import os
import logging
from google.cloud import bigquery
from sklearn.base import BaseEstimator, TransformerMixin


class Quantizer(BaseEstimator, TransformerMixin):
    """
    Classe custom, criada para servir como Transformer que pode ser incluído em um Pipeline do sklearn.

    - Os métodos `__init__()` e `fit()` só são implementados aqui porque são obrigatórios, mas são no-op's.
    - O método principal desta classe é o `transform()`, que tem como objetivo transformar um vetor de números reais em um vetor de números inteiros.
    """

    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """
        Transforma os elementos do vetor X em números inteiros.

        Parameters
        ----------
        X: np.array
            Vetor de números reais.

        Returns
        -------
        np.array: Vetor de números inteiros.

        """
        return (X * 10).astype(int)


def run_query_on_bq(query):
    """
    Executa uma query no BigQuery e retorna os resultados em formato de pandas.DataFrame.

    Returns
    -------
    results : pd.DataFrame
        Dataframe que contém o resultado da query.
    """

    client = bigquery.Client.from_service_account_json(json_credentials_path=os.getenv('GCLOUD_CREDENTIALS_FILEPATH'))
    results_df = client.query(query).to_dataframe()
    return results_df


def df_to_bigquery(df, table_id, credentials_json, job_config=None):
    """
    Salva o dataframe ``df`` na tabela ``table_id`` do BigQuery.

    Parameters
    ----------
    df: pandas.DataFrame
        Dataframe que será salvo no BigQuery

    table_id: str
        Caminho da tabela no BigQuery (projeto.dataset.nome_da_tabela).

    credentials_json: str
        Caminho do arquivo json relacionado à conta de serviço utilizada para conectar ao GCP.

    job_config:
        Opções de configuração para o job que será executado no BigQuery.
    """
    logging.debug('Iniciando salvamento dos dados no BigQuery.')
    client = bigquery.Client.from_service_account_json(credentials_json)
    client.load_table_from_dataframe(df, table_id, job_config=job_config).result()
