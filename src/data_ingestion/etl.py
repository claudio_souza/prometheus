from src.data_ingestion.features import *


def run():
    """
    Executa o processo de ETL, desde a ingestão de dados do BigQuery \
    até a disponibilização dos dados em formato de dataframe (pandas).

    Returns
    -------
    perfis:
        Sequence[pandas.DataFrame]
    """

    logging.debug("Iniciando pipeline de ETL.")

    df_perfil_por_empresa = get_perfil_por_empresa()
    df_perfil_por_plano = get_perfil_por_plano()
    df_perfil_por_operadora = get_perfil_por_operadora()

    logging.debug("Pipeline de ETL finalizado.")

    return df_perfil_por_empresa, df_perfil_por_plano, df_perfil_por_operadora
