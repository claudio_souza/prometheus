from google.cloud import bigquery
from datetime import datetime

import pandas as pd
from src.data_ingestion import etl
from src.data_ingestion.features import *
from src.data_ingestion.utils import Quantizer, df_to_bigquery

import matplotlib.pyplot as plt

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures, MinMaxScaler


def plot_clusters(X, labels, grupo):
    # Criação de um plot 3D dos clusters.
    fig, ax = plt.figure(figsize=(10, 8)), plt.axes(projection='3d')

    x, y, z = X[:, 0], X[:, 1], X[:, 2]

    ax.scatter3D(x, y, z, c=labels)
    plt.title(f'Clusters - {grupo}')

    # Salvar imagens dos clusters para serem displayados na documentação.
    plt.savefig(f'{os.environ["ROOT_DIR"]}/docs/source/img/clusters_{grupo}.png')


def training_pipeline(df: pd.DataFrame, grupo: str = 'empresas') -> pd.DataFrame:
    # Limpeza preliminar do dataframe.
    X = df.fillna(0).drop(
        columns=[
            'idEmpresa', 'dtCompetencia', 'flag_indicador_empresa',
            'IdApoliceSubFaturaPlano', 'dtCompetenciaContapaga', 'flag_indicador_plano',
            'IdOperadora', 'flag_indicador_operadora'
        ],
        errors='ignore'
    )

    # Inicialização do pipeline de pré-processamento.
    preprocessing_pipe = Pipeline([
        ("feature_engineering", PolynomialFeatures(degree=50, include_bias=False, interaction_only=True)),
        ("scale_0_1", MinMaxScaler()),
        ("discretize", Quantizer()),
        ("pca", PCA(n_components=3, random_state=42))
    ])

    # Inicialização do pipeline de clusterização.
    clustering_pipe = Pipeline([
        ("cluster", KMeans(n_clusters=5, n_init=50, max_iter=500, random_state=42))
    ])

    # Pré-processamento + Treino.
    preprocessed_X = preprocessing_pipe.fit_transform(X)
    clusters = clustering_pipe.fit_predict(preprocessed_X)

    # Salvar plot dos clusters.
    plot_clusters(preprocessed_X, clusters, grupo)

    # Adicionar labels ao dataframe original.
    output_df = df.copy()
    output_df['cluster'] = clusters

    return output_df


def round_(x):
    try:
        return round(x, 3)
    except:
        return x


def run():
    logging.debug("Iniciando pipeline de treino.")

    # ETL.
    df_perfil_por_empresa, df_perfil_por_plano, df_perfil_por_operadora = etl.run()

    # Treino.
    benchmark_empresas = training_pipeline(df_perfil_por_empresa)
    benchmark_planos = training_pipeline(df_perfil_por_plano, grupo='planos')
    benchmark_operadoras = training_pipeline(df_perfil_por_operadora, grupo='operadoras')

    # Montar resultado.
    benchmark_df = pd.concat([benchmark_empresas, benchmark_planos, benchmark_operadoras])
    benchmark_df['timestamp'] = pd.to_datetime(datetime.now())
    benchmark_df = benchmark_df.applymap(lambda x: round_(x))

    # Salvar resultado no BigQuery.
    job_config = bigquery.LoadJobConfig(
        schema=[
            bigquery.SchemaField("idEmpresa", bigquery.enums.SqlTypeNames.INTEGER),
            bigquery.SchemaField("IdOperadora", bigquery.enums.SqlTypeNames.INTEGER),
            bigquery.SchemaField("IdApoliceSubFaturaPlano", bigquery.enums.SqlTypeNames.INTEGER)
        ]
    )

    df_to_bigquery(
        df=benchmark_df,
        table_id=f"{os.environ['GCLOUD_PROJECT']}.fenix_clean.benchmark",
        credentials_json=os.environ['GCLOUD_CREDENTIALS_FILEPATH'],
        job_config=job_config
    )
