import os


class Config:
    def __init__(self):
        """Estas configurações valem para os ambientes de dev e prod."""
        os.environ['CONFIG_DIR'] = os.path.dirname(os.path.abspath(__file__))
        os.environ['SRC_DIR'] = os.path.dirname(os.getenv('CONFIG_DIR'))
        os.environ['ROOT_DIR'] = os.path.dirname(os.getenv('SRC_DIR'))


class ConfigDev(Config):
    def __init__(self):
        """Estas configurações valem apenas para o ambiente de dev."""
        super().__init__()
        os.environ['GCLOUD_PROJECT'] = 'laboratorio-eng-dados'
        os.environ['GCLOUD_CREDENTIALS_FILEPATH'] = os.path.join(os.environ['CONFIG_DIR'], 'gcloud_credentials_dev.json')


class ConfigPrd(Config):
    def __init__(self):
        """Estas configurações valem apenas para o ambiente de prod."""
        super().__init__()
        os.environ['GCLOUD_PROJECT'] = 'cosmic-shift-235317'
        os.environ['GCLOUD_CREDENTIALS_FILEPATH'] = os.path.join(os.environ['CONFIG_DIR'], 'gcloud_credentials_prd.json')
