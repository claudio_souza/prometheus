import flask
import logging
from datetime import datetime
from src.config.config import ConfigDev
from src.config.config import ConfigPrd


# Configurar logging.
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s --> %(levelname)s:  %(message)s',
    datefmt='%d/%m/%y %H:%M:%S'
)

# Inicializar aplicação Flask e apontar caminho onde a documentação é gerada.
app = flask.Flask(__name__, static_url_path='/', static_folder='docs/build/html/')


@app.route('/', methods=['GET'])
def serve_sphinx_docs(path='index.html'):
    return app.send_static_file(path)


@app.route('/', methods=['POST'])
def start():
    ConfigPrd() if ('prd-prometheus' in flask.request.url) else ConfigDev()

    # Eu sei que fazer import dentro do código é feio, mas é a única forma de eu configurar
    # o runtime environment antes de continuar com o resto do código.
    from src import train
    train.run()

    logging.debug(f"Pipeline de treino finalizado em {datetime.now()}.")
    return '', 204
