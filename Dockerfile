FROM python:3.8-slim

# Disable Python's stdout/stderr buffering.
ENV PYTHONUNBUFFERED False

# Set path for python project.
ENV PROJECT_HOME $HOME/prometheus
COPY . $PROJECT_HOME
WORKDIR $PROJECT_HOME

# Install python dependencies.
RUN pip install -r requirements.txt

# Build documentation.
RUN sphinx-build -b html docs/source/ docs/build/html

# Start server on port :8080
EXPOSE 8080
CMD exec gunicorn --bind :8080 --log-level=debug --timeout 0 app:app